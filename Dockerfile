FROM gcr.io/dcs-analytics-257714/dcs_mapping_base:latest

COPY ./coord_robot /app/coord_robot
COPY main.py /app

RUN echo "Downloading vector files..." && \
    gsutil cp \
    gs://dcs-vector-data/caucasus.tar.gz /app/ && \
  tar -xvf /app/caucasus.tar.gz
