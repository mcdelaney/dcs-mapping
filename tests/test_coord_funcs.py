import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import pytest
from coord_robot.core import PositionResolver


@pytest.fixture
def position_resolver():
    """Fixture to generate a database."""
    from pathlib import Path
    return PositionResolver(Path("./vec_geojson/caucasus"))


def test_resolve_point(position_resolver):
    result = position_resolver.check_contained(42.0490, 42.0743)
    assert tuple(["urban"]) == result

    result = position_resolver.check_contained(41.68821, 43.50718)
    for val in ["rivers", "vegetation"]:
        assert val in result
