# dcs-mapping

## TLDR  
Use this to get terrain attributes from an arbitrary coordinate inside the DCS Caucasus map.
The data served reflects the _DCS_ caucasus map, and not the actual Caucasus.  
If you are a real-world cartographer, go elsewhere.  
If you are a DCS mission maker who is tired of creating ground units inside of forests, you've come to the right place.  
Serving at: http://dcs-mapping.info/  (No auth or anything...don't make me regret this)   
Example request: <http://dcs-mapping.info/coord_attributes?lat=42.0490&lon=42.0743>

## Summary  
This project, so far, serves a single endpoint: `/coord_attributes?lat={float}&lon={float}`.  
Requests to this endpoint return an array of terrain elements for for whom the input coordinate fall inside the boundary.  
Possible terrain elements are:
- urban
- water
- road
- rail
- vegetation

## Data  
The data is publicly available from the following GCS bucket: `gs://dcs-vector-data/caucasus.tar.gz`.  
The data were originally provided by DCS forum user "flappiefh" [here](https://www.digitalcombatsimulator.com/en/files/3305623/), but have been restructured for ease of use in GeoJSON vector format.  
Additionally, roads, which were originally represented as point-strings, have been buffered as polygons.  

## Developing/Running Locally
To start the server using Docker, run: `./scripts/build_and_run.sh`.  
This will build the image, and start the server on `127.0.0.1`.  
Then, make a request:
```
curl http://127.0.0.1/coord_attributes?lat=41.68821&lon=43.50718
```
Auto-generated documentation can be viewed at `http://127.0.0.1/docs`.  

If you would like to run the application locally without Docker:  
1. Download the vector files from `gs://dcs-vector-data/caucasus.tar.gz` and extract
them in the project root.
2. Create a virtual environment (python 3.7 required) with the required dependencies.
E.g:
```
virtualenv env --python python3.7 && source env/bin/activate && pip install -r requirements.txt
```
3. Start the server with:
```
uvicorn main:app --reload
```

## Tests  
To run tests:
```
pytest .
```
NOTE: In an effort to comply with standard Eagle Dynamics software development practices,
a single (1) test is provided.  
Requests for additional tests are likely to be be rejected unless submitted with a short track.

## Deployment
The project runs on k8's cluster inside GCP project `dcs-analytics-257714`.  
Deployment automated using Gitlab CI/CD.  
Commits to non-master branches will test and build images, but not push.  
Commits to master will do the same, but will update the deployment image-id in prod.
