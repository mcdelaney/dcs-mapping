"""
Flask app to serve coordinate data.
"""
import logging
from pathlib import Path
import json

from fastapi import FastAPI

from coord_robot.core import PositionResolver
from coord_robot.core import get_logger

DATA_PATH = Path('./vec_geojson/caucasus/')


class CoordServer(FastAPI):
    def __init__(self, *kwargs, **args):
        FastAPI.__init__(self, "CoordServer")
        self.logger = get_logger("coord_server")

app = CoordServer("CoordServer")
attr_finder = PositionResolver(geojson_dir=DATA_PATH)


@app.get("/healthz")
def healthz():
    return "ok"

@app.get("/")
def read_root():
    return {"Coordinate": "Robot"}

@app.get("/coord_attributes/")
def coord_attributes(lat: float, lon: float):
    return attr_finder.check_contained(lat, lon)
