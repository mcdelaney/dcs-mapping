import logging
from pathlib import Path

from coord_robot.core.coord_funcs import *


def get_logger(name):
    """Create a logger for the server."""
    logFormatter = logging.Formatter(
        "%(asctime)s [%(name)s] [%(levelname)-5.5s]  %(message)s")
    file_path = Path(f"log/{name}.log")
    if not file_path.parent.exists():
        file_path.parent.mkdir()
    fileHandler = logging.FileHandler(file_path, 'w')
    fileHandler.setFormatter(logFormatter)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)

    logger = logging.getLogger(name=name)
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)
    logger.setLevel(level=logging.INFO)
    logger.propogate = False
    return logger
