"""Coordinate processing

This module defines classes and methods to determine attributes of a given coordinate,
or retrieve coordsinates with specificed attributes.

Example:
    Determine whether a given coordinate overlaps with any terrain elements.
    coord_data = PositionResolver("./vec_geojson/")
    ["river"] = coord_data.get_terrain_attributes(Coordinate(44.00010, 39.1001))
"""
import datetime as dt
import logging
from pathlib import Path
from typing import List, Dict, Tuple, Set

import rapidjson as json
from shapely.geometry import shape, Point, MultiPolygon, LineString
from shapely.prepared import prep


def getLogger(name):
    logFormatter = logging.Formatter(
    "%(asctime)s [%(name)s] [%(levelname)-5.5s]  %(message)s")
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger = logging.getLogger(name)
    logger.addHandler(consoleHandler)
    logger.propagate = False
    logger.setLevel(level=logging.INFO)
    return logger



class PositionResolver:
    """Methods to load and refence geojson data."""
    def __init__(self, geojson_dir: Path):
        self.log = getLogger(__name__)
        if not geojson_dir.exists:
            raise FileExistsError(f"The path {geojson_dir} does not exist")
        self.geojson_dir = geojson_dir
        self.vec_data = self.load()


    def load(self) -> Dict:
        """
        Recurse geodata_dir and read any geojson files, returing a dict
        with filename stems as key.
        Args:
            None
        Returns:
            dict
        """
        t1 = dt.datetime.now()
        data = {}
        for geo_file in self.geojson_dir.iterdir():
            self.log.info("Loading data from path: %s" % geo_file)
            key, value = self.read_single_geojson(geo_file)
            if self.ensure_data_polygon(value):
                multi_poly = self.geoms_to_shapes(value)
                if multi_poly:
                    data[key] = multi_poly
            else:
                self.log.warning("Dropping %s as it has 0 polygon geometry elems...",
                                 key)
        t2 = dt.datetime.now()
        self.log.info("Geo data loaded in %0.4f seconds",
                      (t2-t1).total_seconds())
        return data

    def geoms_to_shapes(self, geo_data: Dict) -> MultiPolygon:
        """Convert geojson geoms to shapes."""
        self.log.info("Converting geometry elements to shapes...")
        poly_list = []
        for ft in geo_data['features']:

            try:
                poly = shape(ft['geometry'])
            except (ValueError, IndexError):
                continue
            if isinstance(poly, Point):
                # Points cant be part of multipoly.
                # If we can about detecting proximity, we could buffer them.
                continue

            if isinstance(poly, MultiPolygon):
                poly_list += poly
            else:
                poly_list.append(poly)
        if poly_list:
            try:
                self.log.info("Coercing to multipoly....")
                return prep(MultiPolygon(poly_list))
            except IndexError:
                pass
        else:
            None

    @staticmethod
    def read_single_geojson(geo_file: Path) -> Tuple:
        """
        Read a single geojson file for the purpose of populating self.vec_data

        Args:
            geo_file (Path): Location of geojson file to read.

        Returns:
            Tuple(str, Dict)
        """
        if geo_file.suffix != ".geojson":
            raise TypeError("File %s is not a geojson file!" % geo_file)

        with geo_file.open('r') as fp_:
            value = json.load(fp_)
        return (geo_file.stem, value)

    def ensure_data_polygon(self, data) -> bool:
        """
        Given a geojson dict, ensure that it includes one or more polygon geometry
        elements.
        """
        try:
            for feature in data['features']:
                if feature['geometry']["type"] in ["MultiPolygon"]:
                    return True
        except (KeyError, TypeError):
            return False
        return True

    def check_contained(self, lat: float, lon: float) -> Tuple:
        """Given a point, check if it falls inside of any vector polygons."""
        self.log.info("Checking point for terrain overlap...")
        t1 = dt.datetime.now()
        matched_types = set()
        point = Point(lat, lon)
        for geo_attrib, geo_data in self.vec_data.items():
            if geo_data.contains(point):
                matched_types.add(geo_attrib)
        t2 = dt.datetime.now()
        self.log.info("Check completed in: %0.4f seconds", (t2-t1).total_seconds())
        return tuple(matched_types)
