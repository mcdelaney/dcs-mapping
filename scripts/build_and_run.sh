#!/usr/bin/env bash

docker build -t dcs_mapping . && \
  docker run  -d -p 80:80 dcs_mapping
